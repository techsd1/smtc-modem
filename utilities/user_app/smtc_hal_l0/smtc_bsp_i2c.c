/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2021 Miromico AG
 * All rights reserved.
 */

/*
 * -----------------------------------------------------------------------------
 * --- DEPENDENCIES ------------------------------------------------------------
 */

#include <stdint.h>   // C99 types
#include <stdbool.h>  // bool type

#include "stm32l0xx_hal.h"
#include "stm32l0xx_ll_i2c.h"
#include "stm32l0xx_hal_i2c.h"
// #include "smtc_bsp_gpio_pin_names.h"
#include "smtc_bsp_i2c.h"
// #include "smtc_bsp_mcu.h"
// #include "smtc_bsp_dbg_trace.h"
#include "stm32l071xx.h"
#include "smtc_hal_mcu.h"
/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE MACROS-----------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE CONSTANTS -------------------------------------------------------
 */
static const uint16_t DELAY_I2C_TX_RX = 5000;
/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE TYPES -----------------------------------------------------------
 */
//I2C1: PB6 SCL, PB9 SDA [AF4] devkit pullup resistors are there
typedef struct bsp_spi_s
{
    I2C_TypeDef*      interface;
    I2C_HandleTypeDef handle;
    struct
    {
        hal_gpio_pin_names_t scl;
        hal_gpio_pin_names_t sda;
    } pins;
} bsp_i2c_t;

/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE VARIABLES -------------------------------------------------------
 */

#if defined( __GNUC__ )
#pragma GCC diagnostic ignored "-Wmissing-braces"
#endif
static bsp_i2c_t bsp_i2c[] = {
    [0] =
        {
            .interface = I2C1,
            .handle    = NULL,
            .pins =
                {
                    .scl = NC,
                    .sda = NC,
                },
        },
    [1] =
        {
            .interface = I2C2,
            .handle    = NULL,
            .pins =
                {
                    .scl = NC,
                    .sda = NC,
                },
        },
};

/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE FUNCTIONS DECLARATION -------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * --- PUBLIC FUNCTIONS DEFINITION ---------------------------------------------
 */


void bsp_i2c_init( const uint32_t id, const hal_gpio_pin_names_t scl, const hal_gpio_pin_names_t sda) {
    // SMTC_HAL_TRACE_PRINTF("bsp_i2c_init\n");

    assert_param( ( id > 0 ) && ( ( id - 1 ) < sizeof( bsp_i2c ) ) );

    uint32_t local_id = id - 1;

    bsp_i2c[local_id].handle.Instance               = bsp_i2c[local_id].interface;
    bsp_i2c[local_id].handle.Init.Timing            = 0x00402535; // 100khz clk with calculator tool from ST site with 10MHz peripheral clk
    // bsp_i2c[local_id].handle.Init.Timing            = 0x00201D2B; //100khz with 8Mhz source frequency, try 0x2000090E from STM32cube
    bsp_i2c[local_id].handle.Init.AddressingMode    = I2C_ADDRESSINGMODE_7BIT;
    bsp_i2c[local_id].handle.Init.DualAddressMode   = I2C_DUALADDRESS_DISABLE;
    bsp_i2c[local_id].pins.scl = scl;
    bsp_i2c[local_id].pins.sda = sda;

    if( HAL_I2C_Init( &bsp_i2c[local_id].handle ) != HAL_OK )
    {
    SMTC_HAL_TRACE_PRINTF("mcu_panic\n");
        mcu_panic( );
    }
    __HAL_I2C_ENABLE( &bsp_i2c[local_id].handle );
    // SMTC_HAL_TRACE_PRINTF("done\n");
}


void bsp_i2c_deinit( const uint32_t id )
{
    assert_param( ( id > 0 ) && ( ( id - 1 ) < sizeof( bsp_i2c ) ) );
    uint32_t local_id = id - 1;

    HAL_I2C_DeInit( &bsp_i2c[local_id].handle );
}

void bsp_i2c_txrx (const uint32_t id, uint16_t DevAddress, uint8_t *txData, uint16_t txSize,
        uint8_t *rxData, uint16_t rxSize, uint32_t Timeout) {

    uint32_t local_id = id - 1;

    // SMTC_HAL_TRACE_PRINTF("bsp_i2c_txrx: id: %x, addr: %x, size: %x, timeout: %x \n", id, DevAddress, txSize, Timeout);
    HAL_StatusTypeDef ret;

    ret = HAL_I2C_Master_Transmit(&bsp_i2c[local_id].handle, DevAddress, txData, txSize, Timeout);

    // SMTC_HAL_TRACE_PRINTF("bsp_i2c_txrx: return: %x\n", ret);
    if ( ret == HAL_OK) {
        hal_mcu_wait_us( DELAY_I2C_TX_RX );
        // SMTC_HAL_TRACE_PRINTF("I2C transmit complete \n");
        if(rxSize) HAL_I2C_Master_Receive(&bsp_i2c[local_id].handle, DevAddress, rxData, rxSize, Timeout);
    }
    else{
        SMTC_HAL_TRACE_PRINTF("I2C transmit failed\r\n");
    }

}

void HAL_I2C_MspInit( I2C_HandleTypeDef* i2cHandle )
{
    if( i2cHandle->Instance == bsp_i2c[0].interface )
    {
        //GPIO_TypeDef*    gpio_port = ( GPIO_TypeDef* ) ( APB2PERIPH_BASE + ( ( bsp_i2c[1].pins.scl & 0xF0 ) << 6 ) );
        GPIO_TypeDef*    gpio_port = (GPIO_TypeDef *) GPIOB_BASE;
        GPIO_InitTypeDef gpio      = {
            .Mode      = GPIO_MODE_AF_OD,
            .Pull      = GPIO_NOPULL,
            .Speed     = GPIO_SPEED_HIGH,
            // .Alternate = GPIO_AF4_I2C1,
            .Alternate = 1,
        };
        gpio.Pin = ( 1 << ( bsp_i2c[0].pins.scl & 0x0F ) ) ;

        HAL_GPIO_Init( gpio_port, &gpio );
        gpio.Alternate = 4;
        gpio.Pin = ( 1 << ( bsp_i2c[0].pins.sda & 0x0F ) ) ;
        HAL_GPIO_Init( gpio_port, &gpio );

        __HAL_RCC_I2C1_CLK_ENABLE( );
    }
    else if( i2cHandle->Instance == bsp_i2c[1].interface )
    {
       // GPIO_TypeDef*    gpio_port = ( GPIO_TypeDef* ) ( APB2PERIPH_BASE + ( ( bsp_i2c[1].pins.scl & 0xF0 ) << 6 ) );
        GPIO_TypeDef*    gpio_port = (GPIO_TypeDef *) GPIOB_BASE;
        GPIO_InitTypeDef gpio      = {
            .Mode      = GPIO_MODE_AF_OD,
            .Pull      = GPIO_NOPULL,
            .Speed     = GPIO_SPEED_HIGH,
            .Alternate = GPIO_AF4_I2C1,
        };
        gpio.Pin = ( 1 << ( bsp_i2c[0].pins.scl & 0x0F ) ) | ( 1 << ( bsp_i2c[0].pins.sda & 0x0F ) ) ;

        HAL_GPIO_Init( gpio_port, &gpio );

        __HAL_RCC_I2C2_CLK_ENABLE( );
    }
    else
    {
        mcu_panic( );
    }
}

void HAL_I2C_MspDeInit( I2C_HandleTypeDef* i2cHandle )
{
    uint32_t local_id = 0;
    if( i2cHandle->Instance == bsp_i2c[0].interface )
    {
        __HAL_RCC_I2C1_CLK_DISABLE( );
    }
    else if( i2cHandle->Instance == bsp_i2c[1].interface )
    {
        local_id = 1;
        __HAL_RCC_I2C2_CLK_DISABLE( );
    }
    else
    {
        mcu_panic( );
    }

    HAL_GPIO_DeInit( ( GPIO_TypeDef* ) ( AHBPERIPH_BASE + ( ( bsp_i2c[local_id].pins.scl & 0xF0 ) << 6 ) ),
                     ( 1 << ( bsp_i2c[local_id].pins.scl & 0x0F ) ) | ( 1 << ( bsp_i2c[local_id].pins.sda & 0x0F ) ) );
}
