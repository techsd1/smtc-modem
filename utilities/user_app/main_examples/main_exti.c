/*!
 * \file      main_exti.c
 *
 * \brief     main program for exti example
 *
 * The Clear BSD License
 * Copyright Semtech Corporation 2021. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted (subject to the limitations in the disclaimer
 * below) provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Semtech corporation nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY
 * THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL SEMTECH CORPORATION BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * -----------------------------------------------------------------------------
 * --- DEPENDENCIES ------------------------------------------------------------
 */
#include <stdint.h>   // C99 types
#include <stdbool.h>  // bool type

#include "main.h"

#include "smtc_modem_api.h"
#include "smtc_modem_utilities.h"

#include "smtc_modem_hal.h"
#include "smtc_hal_dbg_trace.h"
#include "lr11xx_wifi.h"
#include "lr11xx_gnss.h"

#include "example_options.h"

#include "smtc_hal_mcu.h"
#include "smtc_hal_gpio.h"

#include "driver_types.h"

#ifdef USE_SHT3x
#include "sht3x.h"
#endif

#if defined( SX128X )
#include "ralf_sx128x.h"
#elif defined( SX126X )
#include "ralf_sx126x.h"
#elif defined( LR11XX )
#include "ralf_lr11xx.h"
#endif

#include <string.h>

#include "smtc_modem_test_api.h"

#ifdef LR11XX
#include "lr11xx_wifi_types.h"
#include "lr11xx_wifi.h"
#endif

#define MIROMICO_DEV_BOARD
#ifdef MIROMICO_DEV_BOARD
#include "smtc_hal_rtc.h"
#define USER_BUTTON             PA_0
#define USER_LEDB               PC_1
#define USER_LEDG               PH_1
#define USER_LEDR               PH_0
#define BLINK_TIME              100000
#define DEFAULT_USER_INTERVAL   20

static uint32_t user_button_state = 0;


#define USER_CONFIG_FLASH_ADDRESS        0x0802FE00
#define USER_CONFIG_FLASH_ADDRESS_END    0x0802FEFC //1 page (128 B)

#define USER_CONFIG_BASE USER_CONFIG_FLASH_ADDRESS
#define USER_CONFIG_SIZE (0x100)
#define KEYS_CONFIG_BASE (USER_CONFIG_FLASH_ADDRESS + USER_CONFIG_SIZE)
#define KEYS_CONFIG_SIZE 0x100

typedef struct {
    uint8_t     deveui[8];
    uint8_t     joineui[8];
    uint8_t     nwkey[16];
    uint8_t     region;
    uint32_t    samplePeriod;
    uint8_t     majorVersion;
    uint8_t     minorVersion;

} settings_t;
settings_t *keys_settings = (settings_t*)(KEYS_CONFIG_BASE);
#endif


/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE CONSTANTS -------------------------------------------------------
 */

/**
 * Stack id value (multistacks modem is not yet available)
 */
#define STACK_ID 0

/**
 * @brief Stack credentials
 */
static const uint8_t user_dev_eui[8]  = USER_LORAWAN_DEVICE_EUI;
static const uint8_t user_join_eui[8] = USER_LORAWAN_JOIN_EUI;
static const uint8_t user_app_key[16] = USER_LORAWAN_APP_KEY;

#if defined( SX128X )
const ralf_t modem_radio = RALF_SX128X_INSTANTIATE( NULL );
#elif defined( SX126X )
const ralf_t modem_radio = RALF_SX126X_INSTANTIATE( NULL );
#elif defined( LR11XX )
const ralf_t modem_radio = RALF_LR11XX_INSTANTIATE( NULL );
#else
#error "Please select radio board.."
#endif
/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE TYPES -----------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE VARIABLES -------------------------------------------------------
 */
static volatile bool user_button_is_press = false;  // Flag for button status
static uint8_t       rx_payload[255]      = { 0 };  // Buffer for rx payload
static uint8_t       rx_payload_size      = 0;      // Size of the payload in the rx_payload buffer

eDeviceStatus_t  status;
#ifdef USE_STS31
sts31_data       tempsens;
#endif
#ifdef USE_SHT21
sht21_data       tempsens;
#endif
#ifdef USE_SHT3x
sht3x_data       tempsens;
#endif
int16_t          actual_temperature=0; //in 0.01°C



static uint32_t sleep_time_ms = 0;
static uint32_t rtc_time_loop_now_ms = 0;
static uint32_t rtc_time_loop_last_ms = 0;
static uint32_t rtc_time_measurement_last_ms = 0;
static uint32_t time_since_last_measurement_ms = 0;
static uint32_t time_since_last_loop_ms = 0;
static uint32_t remaining_measurement_period_ms = 0;
static uint32_t tx_pending = 0;

#define USER_CONFIG_MAGIC 0xABCDEFEF
typedef struct { // 64 bit blocks
  uint32_t magic;
  uint32_t interval; //in seconds
} user_config_t;
user_config_t user_config;

/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE FUNCTIONS DECLARATION -------------------------------------------
 */
static bool is_joined( void );
static void get_event( void );
static void user_button_callback( void* context );

static void sensor_read(void) {
    SMTC_HAL_TRACE_PRINTF("----- sensor_read -----\n");
    // bsp_mcu_get_bat_adc();
    sht3x_read(&status, &tempsens);
    actual_temperature = tempsens.temp;
    SMTC_HAL_TRACE_PRINTF( "temperature: %d.%d\n", actual_temperature/1000, actual_temperature%1000);
}

static void tx_measurement_data_keep_alive()
{
    if (tx_pending == 1)
    {
        SMTC_HAL_TRACE_PRINTF("tx_pending, failed to send tx_measurement_data_keep_alive\n");
        return;
    }

    uint8_t tx[50];
    tx[0] = actual_temperature >> 8;
    tx[1] = actual_temperature;

    smtc_modem_return_code_t ret;
    ret = smtc_modem_request_uplink( STACK_ID, 7, false, tx, 2 ); //port 7
    SMTC_HAL_TRACE_PRINTF("ret: %x\n", ret);
    tx_pending = 1;
}

static void lr1110_modem_reset_event( uint16_t reset_count );

void wifi_scan(void)
{
    SMTC_HAL_TRACE_PRINTF("\nWIFI Scan-----------------\n");
    uint32_t i;
    uint32_t wifi_counter = 0;
    // lr11xx_wifi_basic_complete_result_t wifi_scan_result;
    // lr11xx_wifi_basic_mac_type_channel_result_t wifi_scan_result;
    lr11xx_wifi_extended_full_result_t wifi_scan_result;
    lr11xx_status_t ret;
    uint8_t nb_results;
    ret = lr11xx_wifi_scan(modem_radio.ral.context, LR11XX_WIFI_TYPE_SCAN_B_G_N, 0x3FFF, LR11XX_WIFI_SCAN_MODE_UNTIL_SSID, 12, 3, 110, true);

    uint32_t now = hal_rtc_get_time_ms();
    ret = lr11xx_wifi_get_nb_results( modem_radio.ral.context, &nb_results );
    if(nb_results > 0){
        SMTC_HAL_TRACE_PRINTF("Listing WIFI networks: %d\n", nb_results);
    }
    else{
        SMTC_HAL_TRACE_PRINTF("No WIFI networks found: %d\n", nb_results);
    }
    for (i = wifi_counter; i < nb_results; i++){
        // ret = lr11xx_wifi_read_basic_complete_results( modem_radio.ral.context, i, 1, &wifi_scan_result);
        // ret = lr11xx_wifi_read_basic_mac_type_channel_results( modem_radio.ral.context, i, 1, &wifi_scan_result);
        ret = lr11xx_wifi_read_extended_full_results( modem_radio.ral.context, i, 1, &wifi_scan_result);
        // SMTC_HAL_TRACE_PRINTF("wifi %d: mac:%02x:%02x:%02x:%02x:%02x:%02x, rssi: %d\n", i, wifi_scan_result.mac_address[0], wifi_scan_result.mac_address[1], wifi_scan_result.mac_address[2], wifi_scan_result.mac_address[3], wifi_scan_result.mac_address[4], wifi_scan_result.mac_address[5], wifi_scan_result.rssi);
        SMTC_HAL_TRACE_PRINTF("wifi %d: SSID: %s, ", i, wifi_scan_result.ssid_bytes);
        // SMTC_HAL_TRACE_PRINTF("mac1: %02x:%02x:%02x:%02x:%02x:%02x, rssi: %d\n", i, wifi_scan_result.mac_address_1[0], wifi_scan_result.mac_address_1[1], wifi_scan_result.mac_address_1[2], wifi_scan_result.mac_address_1[3], wifi_scan_result.mac_address_1[4], wifi_scan_result.mac_address_1[5], wifi_scan_result.rssi);
        // SMTC_HAL_TRACE_PRINTF("mac2: %02x:%02x:%02x:%02x:%02x:%02x, rssi: %d\n", i, wifi_scan_result.mac_address_2[0], wifi_scan_result.mac_address_2[1], wifi_scan_result.mac_address_2[2], wifi_scan_result.mac_address_2[3], wifi_scan_result.mac_address_2[4], wifi_scan_result.mac_address_2[5], wifi_scan_result.rssi);
        SMTC_HAL_TRACE_PRINTF("mac3: %02x:%02x:%02x:%02x:%02x:%02x, rssi: %d\n", i, wifi_scan_result.mac_address_3[0], wifi_scan_result.mac_address_3[1], wifi_scan_result.mac_address_3[2], wifi_scan_result.mac_address_3[3], wifi_scan_result.mac_address_3[4], wifi_scan_result.mac_address_3[5], wifi_scan_result.rssi);
    }
    wifi_counter = nb_results;
}

void gps_snap(void)
{
    SMTC_HAL_TRACE_PRINTF("\nGPS snap----------------\n");
    lr11xx_status_t ret;
    uint8_t nb_sat;
    SMTC_HAL_TRACE_PRINTF("lr11xx_gnss_scan_autonomous...\n");
    ret = lr11xx_gnss_scan_autonomous( modem_radio.ral.context, 1338595200, LR11XX_GNSS_OPTION_BEST_EFFORT, 1, 6);
    hal_mcu_wait_us( 2000000 );
    ret = lr11xx_gnss_get_nb_detected_satellites( modem_radio.ral.context, &nb_sat );
    SMTC_HAL_TRACE_PRINTF("number of satellites found: %d\n\n", nb_sat);
}

void button_led_test(void)
{
    //Init LED
    hal_gpio_init_out( USER_LEDR, 1 );
    hal_gpio_init_out( USER_LEDG, 1 );
    hal_gpio_init_out( USER_LEDB, 1 );
    hal_gpio_set_value( USER_LEDR, 1);
    hal_gpio_set_value( USER_LEDG, 1);
    hal_gpio_set_value( USER_LEDB, 1);

    //Init Button
    hal_gpio_init_in( USER_BUTTON, BSP_GPIO_PULL_MODE_UP, BSP_GPIO_IRQ_MODE_FALLING, NULL );
    user_button_state = hal_gpio_get_value(USER_BUTTON);
    SMTC_HAL_TRACE_PRINTF("UserButtonState: %x\n", user_button_state);

    //If Button Pressed, then LED becomes blue and the program is waiting until the button is released
    if(user_button_state == 0){
        SMTC_HAL_TRACE_PRINTF("UserButton pressed\n");
        hal_gpio_set_value(USER_LEDB, 0);
        hal_mcu_wait_us( BLINK_TIME );
        while(hal_gpio_get_value(USER_BUTTON) == 0);

        hal_gpio_set_value(USER_LEDB, 1);
    }

    //Display nice RGB pattern
    SMTC_HAL_TRACE_INFO( "LED Test\n" );
    SMTC_HAL_TRACE_INFO( "LED R\n" );
    hal_gpio_set_value(USER_LEDR, 0);
    hal_mcu_wait_us( BLINK_TIME );
    hal_gpio_set_value(USER_LEDR, 1);
    SMTC_HAL_TRACE_INFO( "LED G\n" );
    hal_gpio_set_value(USER_LEDG, 0);
    hal_mcu_wait_us( BLINK_TIME );
    hal_gpio_set_value(USER_LEDG, 1);
    SMTC_HAL_TRACE_INFO( "LED B\n" );
    hal_gpio_set_value(USER_LEDB, 0);
    hal_mcu_wait_us( BLINK_TIME );
    hal_gpio_set_value(USER_LEDB, 1);
}

/*
 * -----------------------------------------------------------------------------
 * --- PUBLIC FUNCTIONS DEFINITION ---------------------------------------------
 */

/**
 * @brief Example to send a user payload on an external event
 *
 */
void main_exti( void )
{
    // Disable IRQ to avoid unwanted behaviour during init
    hal_mcu_disable_irq( );

    // Configure all the µC periph (clock, gpio, timer, ...)
    hal_mcu_init( );
    SMTC_HAL_TRACE_PRINTF( "Evaluation Kit Firmware for FMLR STM LR1110\n");
    uint32_t now = hal_rtc_get_time_ms();
    hal_mcu_wait_us( BLINK_TIME );
    SMTC_HAL_TRACE_PRINTF( "Busy wait test: %d\n", hal_rtc_get_time_ms() - now);


    user_config.magic = USER_CONFIG_MAGIC;
    user_config.interval = DEFAULT_USER_INTERVAL;

#ifdef USE_SHT3x
    sht3x_init(&status);
    if (status == eDevice_Initialized){
        SMTC_HAL_TRACE_PRINTF("sht31: init done\n");
    } else{
        SMTC_HAL_TRACE_PRINTF("sht31: init fail\n");

    }
    sht3x_read(&status, &tempsens);
    actual_temperature = tempsens.temp;
#endif

    SMTC_HAL_TRACE_PRINTF( "temperature: %d.%d\n", actual_temperature/1000, actual_temperature%1000);

    // // Init the modem and use get_event as event callback, please note that the callback will be
    // // called immediatly after the first call to smtc_modem_run_engine because of the reset detection
    smtc_modem_init( &modem_radio, &get_event );

    //continuous wave output
    // modem_set_test_mode_status(true);
// #define BOOT_CW    
#ifdef BOOT_CW    
    SMTC_HAL_TRACE_PRINTF( "smtc_modem_test_start\n");
    smtc_modem_test_start();
    uint32_t cw_freq = 868000000;
    int8_t tx_power = 10;
    SMTC_HAL_TRACE_PRINTF( "Continuous Wave Output\n- Frequency: %d Hz\n- TxPower: %d dBm\n", cw_freq, tx_power);
    smtc_modem_return_code_t ret = smtc_modem_test_tx_cw( cw_freq, tx_power );
    SMTC_HAL_TRACE_PRINTF("ret: %d\nwhile(true);\n", ret);
    while(true);
#endif

#define TEST_LED_BUTTON    
#ifdef TEST_LED_BUTTON  
    button_led_test();
#endif

#define BOOT_WIFI_SCAN
#ifdef BOOT_WIFI_SCAN
    wifi_scan();
#endif

#define GPS_SNAP
#ifdef GPS_SNAP
    gps_snap();
#endif



    // Configure DevBoard button as EXTI
    hal_gpio_irq_t user_button = {
        .context  = NULL,                  // context pass to the callback - not used in this example
        .callback = user_button_callback,  // callback called when EXTI is triggered
    };

    // hal_gpio_init_in( PC_13, BSP_GPIO_PULL_MODE_NONE, BSP_GPIO_IRQ_MODE_FALLING, &user_button );
    hal_gpio_init_in( USER_BUTTON, BSP_GPIO_PULL_MODE_NONE, BSP_GPIO_IRQ_MODE_FALLING, &user_button );

    // Re-enable IRQ
    hal_mcu_enable_irq( );

    SMTC_HAL_TRACE_INFO( "EXTI example is starting \n" );

    while( 1 )
    {
        // SMTC_HAL_TRACE_INFO( "ms: %d\n", hal_rtc_get_time_100us() );
        // Execute modem runtime, this function must be recalled in sleep_time_ms (max value, can be recalled sooner)
        sleep_time_ms = smtc_modem_run_engine( );

        // Book-Keeping of timers
        rtc_time_loop_now_ms = hal_rtc_get_time_ms();

        //Calculate time since last wake-up
        if (rtc_time_loop_now_ms >= rtc_time_loop_last_ms){
            time_since_last_loop_ms = rtc_time_loop_now_ms - rtc_time_loop_last_ms;
        } else {
            SMTC_HAL_TRACE_PRINTF("loop timer wrap around\n");
            time_since_last_loop_ms = 0; //Todo Wrap around;
        }

        //Calculate time since last measurement
        if (rtc_time_loop_now_ms < rtc_time_measurement_last_ms){
            rtc_time_measurement_last_ms = 0;
        }
        time_since_last_measurement_ms = rtc_time_loop_now_ms - rtc_time_measurement_last_ms;


        //Calculate remaining measurement time
        if (time_since_last_measurement_ms >= 1000*user_config.interval){
            remaining_measurement_period_ms = 0;
        } else{
            remaining_measurement_period_ms = 1000*user_config.interval - time_since_last_measurement_ms;
        }

        //Handle Measurement Tick
        if (remaining_measurement_period_ms == 0)
        {
            SMTC_HAL_TRACE_PRINTF("user_config.interval (s):%d, rtc_time_measurement_last_ms: %d, now (ms):%d\n", user_config.interval, rtc_time_measurement_last_ms, rtc_time_loop_now_ms);
            sensor_read();
            if (!is_joined()) {
                smtc_modem_join_network( STACK_ID );
            }
            else{
                tx_measurement_data_keep_alive();
            }
            rtc_time_measurement_last_ms = rtc_time_loop_now_ms;
            remaining_measurement_period_ms = 1000*user_config.interval;
        }

        // Determine shortest sleep time
        sleep_time_ms = smtc_modem_run_engine( );

        if ((remaining_measurement_period_ms > 0) && (sleep_time_ms > remaining_measurement_period_ms))
        {
            sleep_time_ms = remaining_measurement_period_ms;
        }

        // Book-Keeping
        rtc_time_loop_last_ms = hal_rtc_get_time_ms();

        SMTC_HAL_TRACE_PRINTF(".");

        // SMTC_HAL_TRACE_INFO( "Sleep for %d ms\n", sleep_time_ms );
        hal_mcu_set_sleep_for_ms(sleep_time_ms);
    }
}

/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE FUNCTIONS DEFINITION --------------------------------------------
 */

/**
 * @brief User callback for modem event
 *
 *  This callback is called every time an event ( see smtc_modem_event_t ) appears in the modem.
 *  Several events may have to be read from the modem when this callback is called.
 */
static void get_event( void )
{
    SMTC_HAL_TRACE_MSG_COLOR( "get_event () callback\n", HAL_DBG_TRACE_COLOR_BLUE );

    smtc_modem_event_t current_event;
    uint8_t            event_pending_count;
    uint8_t            stack_id = STACK_ID;

    // Continue to read modem event until all event has been processed
    do
    {
        // Read modem event
        smtc_modem_get_event( &current_event, &event_pending_count );

        switch( current_event.event_type )
        {
        case SMTC_MODEM_EVENT_RESET:
            SMTC_HAL_TRACE_INFO( "Event received: RESET\n" );

            // Set user credentials
            smtc_modem_set_deveui( stack_id, user_dev_eui );
            smtc_modem_set_joineui( stack_id, user_join_eui );
            smtc_modem_set_nwkkey( stack_id, user_app_key );

            smtc_modem_set_deveui( stack_id, keys_settings->deveui );
            smtc_modem_set_joineui( stack_id, keys_settings->joineui );
            smtc_modem_set_nwkkey( stack_id, keys_settings->nwkey );
            // Set user region
            smtc_modem_set_region( stack_id, MODEM_EXAMPLE_REGION );
            // Schedule a Join LoRaWAN network
            smtc_modem_join_network( stack_id );
            break;

        case SMTC_MODEM_EVENT_ALARM:
            SMTC_HAL_TRACE_INFO( "Event received: ALARM\n" );

            break;

        case SMTC_MODEM_EVENT_JOINED:
            SMTC_HAL_TRACE_INFO( "Event received: JOINED\n" );
            SMTC_HAL_TRACE_INFO( "Modem is now joined \n" );
            break;

        case SMTC_MODEM_EVENT_TXDONE:
            SMTC_HAL_TRACE_INFO( "Event received: TXDONE\n" );
            SMTC_HAL_TRACE_INFO( "Transmission done \n" );
            tx_pending = 0;
            break;

        case SMTC_MODEM_EVENT_DOWNDATA:
            SMTC_HAL_TRACE_INFO( "Event received: DOWNDATA\n" );
            rx_payload_size = ( uint8_t ) current_event.event_data.downdata.length;
            memcpy( rx_payload, current_event.event_data.downdata.data, rx_payload_size );
            SMTC_HAL_TRACE_PRINTF( "Data received on port %u\n", current_event.event_data.downdata.fport );
            SMTC_HAL_TRACE_ARRAY( "Received payload", rx_payload, rx_payload_size );
            break;

        case SMTC_MODEM_EVENT_UPLOADDONE:
            SMTC_HAL_TRACE_INFO( "Event received: UPLOADDONE\n" );

            break;

        case SMTC_MODEM_EVENT_SETCONF:
            SMTC_HAL_TRACE_INFO( "Event received: SETCONF\n" );

            break;

        case SMTC_MODEM_EVENT_MUTE:
            SMTC_HAL_TRACE_INFO( "Event received: MUTE\n" );

            break;

        case SMTC_MODEM_EVENT_STREAMDONE:
            SMTC_HAL_TRACE_INFO( "Event received: STREAMDONE\n" );

            break;

        case SMTC_MODEM_EVENT_JOINFAIL:
            SMTC_HAL_TRACE_INFO( "Event received: JOINFAIL\n" );
            SMTC_HAL_TRACE_WARNING( "Join request failed \n" );
            break;

        case SMTC_MODEM_EVENT_TIME:
            SMTC_HAL_TRACE_INFO( "Event received: TIME\n" );
            break;

        case SMTC_MODEM_EVENT_TIMEOUT_ADR_CHANGED:
            SMTC_HAL_TRACE_INFO( "Event received: TIMEOUT_ADR_CHANGED\n" );
            break;

        case SMTC_MODEM_EVENT_NEW_LINK_ADR:
            SMTC_HAL_TRACE_INFO( "Event received: NEW_LINK_ADR\n" );
            break;

        case SMTC_MODEM_EVENT_LINK_CHECK:
            SMTC_HAL_TRACE_INFO( "Event received: LINK_CHECK\n" );
            break;

        case SMTC_MODEM_EVENT_ALMANAC_UPDATE:
            SMTC_HAL_TRACE_INFO( "Event received: ALMANAC_UPDATE\n" );
            break;

        case SMTC_MODEM_EVENT_USER_RADIO_ACCESS:
            SMTC_HAL_TRACE_INFO( "Event received: USER_RADIO_ACCESS\n" );
            break;

        case SMTC_MODEM_EVENT_CLASS_B_PING_SLOT_INFO:
            SMTC_HAL_TRACE_INFO( "Event received: CLASS_B_PING_SLOT_INFO\n" );
            break;

        case SMTC_MODEM_EVENT_CLASS_B_STATUS:
            SMTC_HAL_TRACE_INFO( "Event received: CLASS_B_STATUS\n" );
            break;

        default:
            SMTC_HAL_TRACE_ERROR( "Unknown event %u\n", current_event.event_type );
            break;
        }
    } while( event_pending_count > 0 );
}

/**
 * @brief Join status of the product
 *
 * @return Return 1 if the device is joined else 0
 */
static bool is_joined( void )
{
    uint32_t status = 0;
    smtc_modem_get_status( STACK_ID, &status );
    if( ( status & SMTC_MODEM_STATUS_JOINED ) == SMTC_MODEM_STATUS_JOINED )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * @brief User callback for button EXTI
 *
 * @param context Define by the user at the init
 */
static void user_button_callback( void* context )
{
    SMTC_HAL_TRACE_INFO( "Button pushed\n" );

    ( void ) context;  // Not used in the example - avoid warning

    static uint32_t last_press_timestamp_ms = 0;

    // Debounce the button press, avoid multiple triggers
    if( ( int32_t )( smtc_modem_hal_get_time_in_ms( ) - last_press_timestamp_ms ) > 500 )
    {
        last_press_timestamp_ms = smtc_modem_hal_get_time_in_ms( );
        user_button_is_press    = true;

        // When the button is pressed, the device is likely to be in low power mode. In this low power mode
        // implementation, low power needs to be disabled once to leave the low power loop and process the button
        // action.
        hal_mcu_disable_once_low_power_wait( );
    }
}

/* --- EOF ------------------------------------------------------------------ */
